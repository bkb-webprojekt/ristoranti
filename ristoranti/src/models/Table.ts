export interface Table {
    tableId: number,
    guests: number,
    status: string
}