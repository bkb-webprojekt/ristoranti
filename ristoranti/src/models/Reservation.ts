export interface Reservation {
    reservationID: number;
    status: number;
    tableID: number;
    guests: number;
    reservationName: string;
    reservationMail: string;
    reservationRemark: string;
    dateStart: Date;
    dateEnd: Date;
    dateSend: Date;
}