﻿using MailKit.Net.Smtp;
using MimeKit;
using RistorantiBackend.Models;
using System;
using System.Threading.Tasks;
using ristorantibackend.MailService;


namespace ristorantibackend.MailService

{
    public class MailSender : IMailSender
    {
        private readonly MailConfiguration _mailConfig;

        public MailSender(MailConfiguration emailConfig)
        {
            _mailConfig = emailConfig;
        }

        public void SendEmail(MailMessage message)
        {
            var emailMessage = CreateMailMessage(message);
            Send(emailMessage);
        }

        public async Task SendEmailAsync(MailMessage message)
        {
            var mailMessage = CreateMailMessage(message);
            await SendAsync(mailMessage);
        }

        private MimeMessage CreateMailMessage(MailMessage message)
        {
            var emailMessage = new MimeMessage();
            emailMessage.From.Add(new MailboxAddress(_mailConfig.Username, _mailConfig.Email));
            emailMessage.To.AddRange(message.emailList);
            emailMessage.Subject = message.title;

            //TODO: Mail Design

            var bodyBuilder = new BodyBuilder {HtmlBody = string.Format("{0}",message.content)};
            emailMessage.Body = bodyBuilder.ToMessageBody();
            return emailMessage;
        }

        private void Send(MimeMessage mailMessage)
        {
            using (var client = new SmtpClient())
            {
                try
                {
                    client.Connect(_mailConfig.SmtpServer, _mailConfig.Port, true);
                    client.AuthenticationMechanisms.Remove("XOAUTH2");
                    client.Authenticate(_mailConfig.Email, _mailConfig.Password);

                    client.Send(mailMessage);
                }
                catch
                {
                    throw;
                }
                finally
                {
                    client.Disconnect(true);
                    client.Dispose();
                }
            }
        }

        private async Task SendAsync(MimeMessage mailMessage)
        {
            using (var client = new SmtpClient())
            {
                try
                {
                    await client.ConnectAsync(_mailConfig.SmtpServer, _mailConfig.Port, true);
                    client.AuthenticationMechanisms.Remove("XOAUTH2");
                    await client.AuthenticateAsync(_mailConfig.Email, _mailConfig.Password);

                    await client.SendAsync(mailMessage);
                }
                catch 
                {
                    throw;
                }
                finally
                {
                    await client.DisconnectAsync(true);
                    client.Dispose();
                }
            }
        }

        public async Task SendEmailToCustomer(Reservation reservation) 
        {
            var caption = $"Ihre Reservierung #{reservation.ReservationID} ist bei uns eingegangen!";
            if (reservation.Status == 1)
                caption = $"Ihre Reservierung #{reservation.ReservationID} wurde bestätigt!";
            if (reservation.Status == 2)
                caption = $"Ihre Reservierung #{reservation.ReservationID} wurde abgelehnt!";

            MailMessage mailMessage = new MailMessage(new string[] { reservation.ReservationMail }, caption, MailHelper.GetEmailBodyForReservation(reservation));
            await SendEmailAsync(mailMessage);
            Console.WriteLine("EMAIL SYSTEM: EMAIL SEND");
        }
    }
}
