﻿using System;

namespace ristorantibackend.Dtos
{
    public class ReservationDateDto
    {
        public DateTime DateStart { get; set; }

    }
}
