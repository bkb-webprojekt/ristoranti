using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RistorantiBackend.Models
{
    public class Role
    {
        [Key]
        public int RoleId { get; set; }
        [Required]
        public string Name { get; set; }

        public ICollection<User> Users { get; set; }
    }
}