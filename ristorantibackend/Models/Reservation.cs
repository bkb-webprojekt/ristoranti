﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace RistorantiBackend.Models
{
    public class Reservation
    {
        [Key]
        public int ReservationID { get; set; }
        
        [Required]
        /*  0 = Pending
            1 = Accepted
            2 = Rejected    */
        public int Status { get; set; }

        [JsonIgnore] public Table table { get; set; }

        [Required]
        public int TableID { get; set; }

        [Required]
        public int Guests { get; set; }

        [Required]
        public string ReservationName { get; set; }
        
        [Required]
        public string ReservationMail { get; set; }
        
        public string ReservationRemark { get; set; }
        
        [Required]
        public DateTime DateStart { get; set; }
        
        [Required]
        public DateTime DateEnd { get; set; }

        [Required]
        public DateTime DateSend { get; set; }

    }
}
