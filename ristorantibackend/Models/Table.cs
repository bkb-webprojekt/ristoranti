﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace RistorantiBackend.Models
{
    public class Table
    {
        [Key]
        public int TableId { get; set; }
        [Required]
        public int Seats { get; set; }


        public ICollection<Reservation> reservations { get; set; }

    }
}
