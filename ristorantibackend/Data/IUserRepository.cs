using RistorantiBackend.Models;
using System.Collections.Generic;

namespace RistorantiBackend.Data
{
    public interface IUserRepository
    {
        User Create(User user);
        User GetByEmail(string email);
        User GetById(int id);

        bool DeleteUser(int id);

        List<User> GetUsers();
    }
}