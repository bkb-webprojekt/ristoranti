﻿using RistorantiBackend.Models;
using System;
using System.Collections.Generic;

namespace RistorantiBackend.Data
{
    public interface IDataRepository
    {
        Table GetTableById(int TableId);

        List<Reservation> GetReservationsByStatusID(int StatusID);

        List<Reservation> GetReservationsByDate(DateTime date);
        List<Reservation> GetReservationsList();

        List<Reservation> GetUsedTables(DateTime date);
        Reservation CreateReservation(Reservation reservation);

        Reservation GetReservationByID(int ReservationID);

        bool UpdateReservation(Reservation reservation);
    }
}
