using Microsoft.EntityFrameworkCore;
using RistorantiBackend.Models;
using System.Collections.Generic;
using System.Linq;

namespace RistorantiBackend.Data
{
    public class UserRepository : IUserRepository
    {
        private readonly DatabaseContext _context;

        public UserRepository(DatabaseContext context)
        {
            _context = context;
        }

        public User Create(User user)
        {
            _context.Users.Add(user);
            user.UserId = _context.SaveChanges();

            return user;
        }

        public bool DeleteUser(int id)
        {
            _context.Users.Remove(GetById(id));
            _context.SaveChanges();
            if(GetById(id) == null)
            {
                return true;
            }
            return false;
        }

        public User GetByEmail(string email)
        {
            return _context.Users.FirstOrDefault(u => u.Email == email);
        }

        public User GetById(int id)
        {
            return _context.Users.FirstOrDefault(u => u.UserId == id);
        }

        public List<User> GetUsers()
        {
            return _context.Users.FromSqlRaw(
                "SELECT * " +
                "FROM Users U "
                ).ToList();
        }
    }
}
